import {IS_ERROR_APP} from '../types'

export const isAppError = (isError: any) => ({
  type: IS_ERROR_APP,
  payload: isError
})

