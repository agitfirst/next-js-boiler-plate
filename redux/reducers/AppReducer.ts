import {IS_ERROR_APP} from '../types'

const initialState = {
  isError: false
}

export default (state = initialState, action: any) => {
  switch (action.type) {
    case IS_ERROR_APP:
      return {
        ...state,
        isError: action.payload
      }
    default:
      return initialState
  }
}
