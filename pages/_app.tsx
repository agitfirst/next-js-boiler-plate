
import withRedux from 'next-redux-wrapper';
import { Provider } from 'react-redux';
import {configureStore} from '../redux';

function MyApp({ Component, pageProps, store, ...rest }) {
    return (
        <Provider store={configureStore().store}>
            <Component {...pageProps} />
        </Provider>
    );
}


export default (MyApp);