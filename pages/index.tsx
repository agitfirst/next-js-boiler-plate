import React, {useEffect} from 'react'
import Head from 'next/head';
import Router, {useRouter} from 'next/router';
import 'bootstrap/dist/css/bootstrap.min.css';

const IndexPage = () => {
  const router = useRouter()
  const {store_name} = router.query

  useEffect(() => {
    Router.replace(`/store/${store_name || ''}`)
  }, [])

  return (
    <Head>
    <meta name="robots" content="noindex, nofollow" />
  </Head>
  )
}

export default IndexPage
